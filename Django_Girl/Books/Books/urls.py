"""Books URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import path
from reference.views import CreateAuthorView, UpdateAuthorView, DetailAuthorView, ListAuthorView, DeleteAuthorView
from reference.views import CreateGenreView, UpdateGenreView, DetailGenreView, ListGenreView, DeleteGenreView
from reference.views import CreatePublishingOfficeView, UpdatePublishingOfficeView, DetailPublishingOfficeView
from reference.views import ListPublishingOfficeView, DeletePublishingOfficeView
from core.views import UserHomeTemplateView
from admincore.views import StaffHomeTemplateView


urlpatterns = [
    path('admin/', admin.site.urls),
    path('shop-admin/ref/author/create/', CreateAuthorView.as_view()),
    path('shop-admin/ref/author/update/<int:pk>/', UpdateAuthorView.as_view()),
    path('shop-admin/ref/detailed-author/<int:pk>/', DetailAuthorView.as_view()),
    path('shop-admin/ref/list-authors/', ListAuthorView.as_view()),
    path('shop-admin/ref/author/delete/<int:pk>/', DeleteAuthorView.as_view()),

    path('shop-admin/ref/genre/create/', CreateGenreView.as_view()),
    path('shop-admin/ref/genre/update/<int:pk>/', UpdateGenreView.as_view()),
    path('shop-admin/ref/detailed-genre/<int:pk>/', DetailGenreView.as_view()),
    path('shop-admin/ref/list-genres/', ListGenreView.as_view()),
    path('shop-admin/ref/genre/delete/<int:pk>/', DeleteGenreView.as_view()),

    path('shop-admin/ref/publishing-office/create/', CreatePublishingOfficeView.as_view()),
    path('shop-admin/ref/publishing-office/update/<int:pk>/', UpdatePublishingOfficeView.as_view()),
    path('shop-admin/ref/detailed-publishing-office/<int:pk>/', DetailPublishingOfficeView.as_view()),
    path('shop-admin/ref/list-publishing-office/', ListPublishingOfficeView.as_view()),
    path('shop-admin/ref/publishing-office/delete/<int:pk>/', DeletePublishingOfficeView.as_view()),

    path('staff-admin/', StaffHomeTemplateView.as_view()),
    path('', UserHomeTemplateView.as_view()),
]
