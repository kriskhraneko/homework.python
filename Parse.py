def parse():
    delim = input('Введите используемый разделитель: ')
    result = {}
    try:
        with open('Parse.csv') as file_csv:
            for line in file_csv:
                st = line.strip('\n')
                lis = st.split(delim)
                result[lis[0]] = lis[1:len(st)]
                print(st)
            return result
    except:
        print('Такого файла нет!')
parse()