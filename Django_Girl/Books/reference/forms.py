from django import forms
from reference.models import Author, Genre, Publishing_office, Publishing_year


class AuthorForm(forms.ModelForm):
    class Meta:
        model = Author

        fields = [
            'name',
            'description'
        ]


class GenreForm(forms.ModelForm):
    class Meta:
        model = Genre

        fields = [
            'name',
            'description'
        ]


class PublishingOfficeForm(forms.ModelForm):
    class Meta:
        model = Publishing_office

        fields = [
            'name'
        ]


class PublishingYearForm(forms.ModelForm):
    class Meta:
        model = Publishing_year

        fields = [
            'name'
        ]