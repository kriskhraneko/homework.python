from django.db import models

class Author(models.Model):
    name = models.CharField(
        "Автор",
        max_length=200)
    description = models.CharField(
        "Описание",
        max_length=300,
        null=True)

    def get_view_url(self):
        return "/shop-admin/ref/detailed-author/{}/".format(self.pk)

    def get_update_url(self):
        return "/shop-admin/ref/author/update/{}/".format(self.pk)

    def get_delete_url(self):
        return "/shop-admin/ref/author/delete/{}/".format(self.pk)

    class Meta:
        verbose_name = 'Автор'
        verbose_name_plural = 'Авторы'

    def __str__(self):
        return self.name


class Genre(models.Model):
    name = models.CharField(
        "Жанр",
        max_length=200,
        db_index=True
    )
    description = models.CharField(
        "Описание",
        max_length=500,
        null=True)

    def get_view_url(self):
        return "/shop-admin/ref/detailed-genre/{}/".format(self.pk)

    def get_update_url(self):
        return "/shop-admin/ref/genre/update/{}/".format(self.pk)

    def get_delete_url(self):
        return "/shop-admin/ref/genre/delete/{}/".format(self.pk)

    class Meta:
        verbose_name = 'Жанр'
        verbose_name_plural = 'Жанры'

    def __str__(self):
        return self.name


class Publishing_office(models.Model):
    name = models.CharField(
        "Издательство",
        max_length=300,
        db_index=True
    )

    def get_view_url(self):
        return "/shop-admin/ref/detailed-publishing-office/{}/".format(self.pk)

    def get_update_url(self):
        return "/shop-admin/ref/publishing-office/update/{}/".format(self.pk)

    def get_delete_url(self):
        return "/shop-admin/ref/publishing-office/delete/{}/".format(self.pk)

    class Meta:
        verbose_name = 'Издательство'
        verbose_name_plural = 'Издательства'

    def __str__(self):
        return self.name

class Publishing_year(models.Model):
    name = models.CharField(
        "Год издания",
        max_length=300,
        db_index=True
    )

    def get_view_url(self):
        return "/shop-admin/ref/detailed-publishing-year/{}/".format(self.pk)

    def get_update_url(self):
        return "/shop-admin/ref/publishing-year/update/{}/".format(self.pk)

    def get_delete_url(self):
        return "/shop-admin/ref/publishing-year/delete/{}/".format(self.pk)

    class Meta:
        verbose_name = 'Год издания'
        verbose_name_plural = 'Год издания'

    def __str__(self):
        return self.name
