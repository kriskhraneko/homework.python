# Generated by Django 2.0.9 on 2018-12-05 09:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reference', '0002_auto_20181205_1248'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='publishing_year',
            options={'verbose_name': 'Год издания', 'verbose_name_plural': 'Год издания'},
        ),
        migrations.AlterField(
            model_name='publishing_year',
            name='name',
            field=models.IntegerField(db_index=True, max_length=4),
        ),
    ]
