from django.views.generic import CreateView, UpdateView, DetailView, ListView, DeleteView

from reference.forms import AuthorForm
from reference.models import Author

from reference.forms import GenreForm
from reference.models import Genre

from reference.forms import PublishingOfficeForm
from reference.models import Publishing_office


class CreateAuthorView(CreateView):
    form_class = AuthorForm
    template_name = 'author-ref-create.html'
    success_url = '/shop-admin/ref/list-authors/'

    def get_context_data(self, *args, **kwargs):   #переписывается родительский метод
        context = super().get_context_data(*args, **kwargs)
        context['ref_action'] = 'Добавление нового автора'
        return context


class UpdateAuthorView(UpdateView):
    template_name = 'author-ref-create.html'
    form_class = AuthorForm
    model = Author
    success_url = '/shop-admin/ref/list-authors/'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['ref_action'] = 'Редактирование автора'
        context['temp'] = self.kwargs
        return context


class DetailAuthorView(DetailView):
    template_name = 'detailed-author-ref-create.html'
    model = Author

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['dscr'] = 'Автор'
        return context


class ListAuthorView(ListView):
    template_name = 'list-ref-create.html'
    model = Author

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['dscr'] = 'Список авторов'
        return context

class DeleteAuthorView(DeleteView):
    template_name = 'delete.html'
    form_class = AuthorForm
    model = Author
    success_url = '/shop-admin/ref/list-authors/'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['ref_action'] = 'Удаление автора'
        context['temp'] = self.kwargs
        return context

class CreateGenreView(CreateView):
    form_class = GenreForm
    template_name = 'genre-create.html'
    success_url = '/shop-admin/ref/genre/create/'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['ref_action'] = 'Добавление жанра'
        return context


class UpdateGenreView(UpdateView):
    template_name = 'genre-create.html'
    form_class = GenreForm
    model = Genre
    success_url = '/shop-admin/ref/list-genres/'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['ref_action'] = 'Редактирование жанра'
        context['temp'] = self.kwargs
        return context


class DetailGenreView(DetailView):
    template_name = 'detailed-genre.html'
    model = Genre

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['dscr'] = 'Жанр'
        return context


class ListGenreView(ListView):
    template_name = 'list-ref-create.html'
    model = Genre

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['dscr'] = 'Список жанров'
        return context

class DeleteGenreView(DeleteView):
    template_name = 'delete.html'
    form_class = GenreForm
    model = Genre
    success_url = '/shop-admin/ref/list-genre/'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['ref_action'] = 'Удаление жанра'
        context['temp'] = self.kwargs
        return context


class CreatePublishingOfficeView(CreateView):
    form_class = PublishingOfficeForm
    template_name = 'publishing-office-create.html'
    success_url = '/shop-admin/ref/publishing-office/create/'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['ref_action'] = 'Добавление издательства'
        return context


class UpdatePublishingOfficeView(UpdateView):
    template_name = 'publishing-office-create.html'
    form_class = PublishingOfficeForm
    model = Publishing_office
    success_url = '/shop-admin/ref/list-publishing-office/'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['ref_action'] = 'Редактирование издательства'
        context['temp'] = self.kwargs
        return context


class DetailPublishingOfficeView(DetailView):
    template_name = 'detailed-author-ref-create.html'
    model = Genre

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['dscr'] = 'Издательство'
        return context


class ListPublishingOfficeView(ListView):
    template_name = 'list-ref-create.html'
    model = Genre

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['dscr'] = 'Список издательств'
        return context

class DeletePublishingOfficeView(DeleteView):
    template_name = 'delete.html'
    form_class = PublishingOfficeForm
    model = Publishing_office
    success_url = '/shop-admin/ref/list-publishing-office/'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['ref_action'] = 'Удаление издательства'
        context['temp'] = self.kwargs
        return context

