import os
# 1. Input values.
def getName():
    name = input("Как Вас зовут? ")
    if name == "":
        print("Ошибка: нпопробуйте еще раз.")
        getName()
    return name

def getAge():
    age = int(input("Какой у Вас возраст? "))
    if age < 1:
        print("Ошибка: невозможное значение возраста.")
        getAge()
    return age

def getGender():
    gender = (input("Ваш пол?(Ж/М) ")).upper()
    g_list = ['Ж', 'М']
    if gender not in g_list:
        print("Ошибка: некорректное значение пола.")
        getGender()
    return gender

def getHeight():
    height = float(input("Какой у Вас рост?(с см) "))
    h = height / 100
    if height < 1:
        print("Ошибка: невозможное значение роста.")
        getHeight()
    return h

def getWeight():
    weight = float(input("Какой у Вас вес? "))
    if weight < 1:
        print("Ошибка: невозможное значение веса.")
        getHeight()
    return weight

# add users and BMI
def addUser():
    name = getName()
    age = getAge()
    gender = getGender()
    height = getHeight()
    weight = getWeight()
    bmi = weight / height ** 2

    if gender == "Ж":
        print("\n", "Уважаемая ", name.title())
    else:
        print("\n", "Уважаемый ", name.title())
    print("Ваш возраст: ", age)
    print("Ваш рост: ", height, "см")
    print("Ваш вес: ", weight, "килограмм")
    print("Ваш ИМТ = ", bmi)
    print("\n", "Рекомендации:")

# Indexes BMI, conclusion - greetings and recommendations. and plot
    if gender == 'Ж':
        v_1 = 18
        v_2 = 25
        v_3 = 30
        if bmi < float(v_1):
            print("у Вас недостаточная масса тела. Вам необходима высококаллорийная диета!")
            if age >= 50:
                print("Обратитесь к врачу.")
            else:
                print("Срочно начинайте кушать!")
        elif bmi >= float(v_1) and bmi < float(v_2):
            print("у Вас нормальная масса тела. Продолжайте в том же духе!")
        elif bmi >= float(v_2) and bmi < float(v_3):
            print("у Вас избыточная масса тела. Вам необходима диета и больше пеших прогулок!")
            if age >= 50:
                print("Обратитесь к врачу.")
            else:
                print("Займитесь фитнесом.")
        elif bmi >= float(v_3):
            print("у Вас ожирение. Вам необходима консультация диетолога!")
            if age >= 50:
                print("Срочно обратитесь к врачу.")
            else:
                print("Обязательно сходите к диетологу.")
    else:
        v_1 = 22
        v_2 = 30
        v_3 = 35
        if bmi < float(v_1):
            print("у Вас недостаточная масса тела. Вам необходима высококаллорийная диета!")
            if age >= 50:
                print("Обратитесь к врачу")
            else:
                print("Скорректируйте питание")
        elif bmi >= float(v_1) and bmi < float(v_2):
                print("у Вас нормальная масса тела. Продолжайте в том же духе!")
        elif bmi >= float(v_2) and bmi < float(v_3):
            print("у Вас избыточная масса тела. Вам необходима диета и больше пеших прогулок!")
            if age >= 50:
                print("Обратитесь к врачу.")
            else:
                print("Займитесь спортом!")
        elif bmi >= float(v_3):
            print("у Вас ожирение. Вам необходима консультация диетолога!")
            if age >= 50:
                print("Срочно обратитесь к врачу.")
            else:
                print("Обязательно сходите к диетологу.")
    print('Индексы BMI: ', '\n', 'Меньше ', v_1, '- дифицит массы тела;',
          '\n', 'Между  ', v_1, 'и ', v_2, '- нормальная масса тела;',
          '\n', 'Между ', v_2, 'и ', v_3, '- избыточная масса тела;',
          '\n', 'Больше ', v_3, '- ожирение.')
    line = '0' + '-' * (v_1 - 1) + '_' * (v_2 - v_1 - 1) + '+' * (v_3 - v_2 - 1) + '!' * (50 - v_3 - 1) + '50'
    line = list(line)
    if gender == 'Ж':
        line.insert(int(v_1), '18')
        line.insert(int(v_2), '25')
        line.insert(int(v_3), '30')
    else:
        line.insert(int(v_1), '22')
        line.insert(int(v_2), '30')
        line.insert(int(v_3), '35')
    line.insert(int(bmi), 'X')
    print('X - Ваш индекс массы тела', '\n', ''.join(line))
    d = {'Возраст': age, 'Пол': gender, 'Рост': height, 'Вес': weight, 'ИМТ': bmi}
    return {name: d}

# Commands
generalDict = {}

try:
    file = open("BMI.txt", "r")
except:
    file = open("BMI.txt", "w")
file.close()

while True:
    print("1. Добавить пользователя;")
    print("2. Показать всех пользователей;")
    print("3. Удалить пользователя;")
    print("4. Выбрать пользователя;")
    print("5. Обновить информацию о пользователе.")

    act = int(input("Ваш вариант: "))
    if act == 1:
        dict = addUser()
        with open('BMI.txt', 'a') as out:
            for key, val in dict.items():
                for key in dict.keys():
                    generalDict[key] = dict[key]
                out.write('{}:{}\n'.format(key, val))
    elif act == 2:
        file = open("BMI.txt", 'r')
        print(file.read())
        file.close()
    elif act == 3:
        checkAct3 = True
        while checkAct3:
            if os.stat('BMI.txt').st_size == 0:
                print("\n", "Пользователей нет.")
            else:
                print('Выберите пользователя ')
                d = {}
                i = 1
                with open('BMI.txt') as file:
                    for line in file:
                        key, *value = line.split(':')
                        d[i] = key
                        print(i, d[i])
                        i += 1
                id = input()
                with open('BMI.txt', 'r') as file:
                    line = file.readlines()
                    i = int(id) - 1
                    line[i] = ''
                    print (line)
                new_file = open('BMI.txt', 'w')
                new_file.writelines(line)
                new_file.close()

                response = input('Удалить кого-нибудь еще? (Да / Нет)')
                if response.lower() == 'да':
                    checkAct3 = True
                else:
                    checkAct3 = False
            file.close()
    elif act == 4:
        checkAct4 = True
        while checkAct4:
            print('Выберите пользователя ')
            d = {}
            i = 1
            with open('BMI.txt') as file:
                for line in file:
                    key, value = line.split(':')
                    d[i] = key
                    print(i, d[i])
                    i += 1
            count = input()
            with open('BMI.txt') as file:
                for index, line in enumerate(file):
                    if index + 1 == int(count):
                        a = line
                        print(a)
            response = input('Посмотреть кого-нибудь еще? (Да / Нет)')
            if response.lower() == 'да':
                checkAct4 = True
            else:
                checkAct4 = False
    elif act == 5:
        checkAct5 = True
        while checkAct5:
            print('Выберите пользователя ')
            d = {}
            i = 1
            with open('BMI.txt') as file:
                for line in file:
                    key, value = line.split(':')
                    d[i] = key
                    print(i, d[i])
                    i += 1
            number = input()
            with open('BMI.txt') as file:
                field_ch = input('Выберите поле для изменения \n\'Возраст\'  \n\'Рост\' \n\'Вес\' ')
                change = file.read()
                if field_ch == 'Возраст':
                    change = change.replace()
                elif field_ch == 'Рост':
                    change = change.replace()
                elif field_ch == 'Вес':
                    change = change.replace()
            val_ch = input("Введите значение для поля " + field_ch + ":")
            if field_ch in ['Возраст', 'Рост', 'Вес'] and val_ch != "":
                generalDict[k][field_ch] = int(val_ch)
                print(d[i])
            else:
                checkAct5 = False
            response = input('Изменить данные кого-нибудь еще? (Да / Нет)')
            if response.lower() == 'да':
                checkAct5 = True
            else:
                checkAct5 = False
    else:
        print("Не верно! Попробуйте еще раз!")
