from django.views.generic import TemplateView

class StaffHomeTemplateView(TemplateView):
    template_name = 'staff-home.html'
