from django.contrib import admin

# Register your models here.
from .models import Author, Genre, Publishing_office, Publishing_year

admin.site.register(Author)
admin.site.register(Genre)
admin.site.register(Publishing_office)
admin.site.register(Publishing_year)
