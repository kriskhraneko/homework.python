from django.views.generic import TemplateView


class UserHomeTemplateView(TemplateView):
    template_name = 'user-home.html'
