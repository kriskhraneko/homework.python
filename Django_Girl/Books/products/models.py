# # Create your models here.
# from django.db import models
# from django.utils import timezone
# from reference.models import Author
# from reference.models import Genre
# from reference.models import Publishing_office
# from reference.models import Publishing_year
#
# class Books(models.Model):
#     title = models.CharField(
#         max_length=200,
#         db_index=True,
#         verbose_name="Название")
#
#     description = models.TextField(
#         blank=True,
#         verbose_name="Описание")
#
#     price = models.DecimalField(
#         max_digits=9,
#         decimal_places=2,
#         verbose_name="Цена")
#
#     author = models.ManyToManyField(
#         Author,
#         verbose_name='Автор')
#
#     genre = models.ManyToManyField(
#         Genre,
#         verbose_name="Жанр")
#
#     pages = models.DecimalField(
#         null=True,
#         max_digits=10,
#         decimal_places=3,
#         verbose_name="Количество страниц")
#
#     publishing_office = models.ManyToManyField(
#         Publishing_office,
#         verbose_name="Издательство")
#
#     publish_year = models.ManyToManyField(
#         Publishing_year,
#         verbose_name="Год издания")
#
#     rate = models.DecimalField(
#         null=True,
#         max_digits=2,
#         decimal_places=2,
#         verbose_name="Рейтинг"
#     )
#     stock = models.PositiveIntegerField(verbose_name="На складе")
#     available = models.BooleanField(
#         default=True,
#         verbose_name="Доступен"
#     )
#     created = models.DateTimeField(auto_now_add=True)
#     updated = models.DateTimeField(auto_now=True)
#
#     def publish(self):
#         self.published_date = timezone.now()
#         self.save()
#
#     def __str__(self):
#         return self.title


from django.db import models

from reference.models import Author, Genre, Publishing_office, Publishing_year



class Book(models.Model):
    name = models.CharField(
        "Название",
        max_length=200)
    price = models.DecimalField(
        "Стоимость",
        max_digits=9,
        decimal_places=2)
    authors = models.ManyToManyField(Author, verbose_name="Авторы")
    genres = models.ManyToManyField(Genre, verbose_name="Жанры")
    published_year = models.ManyToManyField(Publishing_year, verbose_name="Год издания")
    pages = models.IntegerField("Страниц")
    age_restriction = models.CharField("Возрастные ограничения", max_length=4)
    publisher = models.ForeignKey(
        Publishing_office,
        verbose_name="Издательство",
        on_delete=models.CASCADE)

    stock = models.IntegerField(
        "Количество книг в наличии",
        default=0)

    rate = models.FloatField(
        "Рейтинг",
        default=0)

    created_date = models.DateTimeField(
        "Дата создания",
        auto_now=False,
        auto_now_add=True)

    updated_date = models.DateTimeField(
        "Дата изменения",
        auto_now=True,
        auto_now_add=False)

    def __str__(self):
        return self.name